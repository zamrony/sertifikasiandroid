package com.example.sertifikasi_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

public class SpalshActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sp = getSharedPreferences("LSP", MODE_PRIVATE);
                String error = sp.getString("SHARED_LOGIN", "");

                // TODO jika belum masuk ke LoginActivity
                if (error.equalsIgnoreCase("") || TextUtils.isEmpty(error)){
                    finishAffinity();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
                // TODO jika sudah nantinya akan masuk ke Home
                else {
                    finishAffinity();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));

                }
            }
        }, 2000);
    }
}
