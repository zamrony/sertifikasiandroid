package com.example.sertifikasi_android;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sertifikasi_android.api.ApiConfig;
import com.example.sertifikasi_android.api.ApiService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText vcUsername;
    private EditText vcPassword;
    private Button btnMasuk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
//        finishAffinity();
//        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiService apiService = ApiConfig.getApiService();
                apiService.login(vcUsername.getText().toString().trim(),
                        vcPassword.getText().toString().trim())
                        .enqueue(new Callback<ResponseBody>() {
                            @SuppressLint("NewApi")
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                if (response.isSuccessful()){
                                    try {
                                        JSONObject var = new JSONObject(response.body().string());
                                        Boolean _error = var.optBoolean("error");
                                        String message = var.optString("error_msg");
                                        if (_error){
                                            Toast.makeText(LoginActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                                        } else {
                                            finishAffinity();
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                            SharedPreferences sharedPreferences = getSharedPreferences("LSP", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("SHARED_LOGIN", message);
                                            editor.apply();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(LoginActivity.this, "Login Gagal", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    private void initView() {
        vcUsername = findViewById(R.id.vc_username);
        vcPassword = findViewById(R.id.vc_password);
        btnMasuk = findViewById(R.id.btn_masuk);
    }
}
