package com.example.sertifikasi_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sertifikasi_android.api.ApiConfig;
import com.example.sertifikasi_android.api.ApiService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {

    private String idBarang;
    private String namaBarang;
    private String imageBarang;
    private String dtdeskripsiBarang;
    private String hargaBarang;
    private String stokBarang;

    private ImageView iconItemName;
    private EditText edtNamaBarang;
    private ImageView iconPriceTag;
    private EditText edtHargaBarang;
    private ImageView iconMinus;
    private ImageView iconPlus;
    private EditText edtStokBarang;
    private EditText deskripsiBarang;
    private ImageView ivPhoto;
    private EditText edtUrlGambar;
    private Button btnCheckUrl;
    private Button btnKirim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        initView();

        // Variabel Untuk set Ke Form
        idBarang = getIntent().getStringExtra("ID_BARANG");
        namaBarang = getIntent().getStringExtra("NAMA_BARANG");
        imageBarang = getIntent().getStringExtra("IMAGE_BARANG");
        dtdeskripsiBarang = getIntent().getStringExtra("DESKRIPSI_BARANG");
        hargaBarang = getIntent().getStringExtra("HARGA_BARANG");
        stokBarang = getIntent().getStringExtra("STOK_BARANG");

        // Set data ke form
        edtNamaBarang.setText(namaBarang);
        edtHargaBarang.setText(hargaBarang);
        edtStokBarang.setText(stokBarang);
        deskripsiBarang.setText(dtdeskripsiBarang);
        edtUrlGambar.setText(imageBarang);

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiService apiService = ApiConfig.getApiService();
                apiService.updateData(idBarang,
                        edtNamaBarang.getText().toString().trim(),
                        edtUrlGambar.getText().toString().trim(),
                        deskripsiBarang.getText().toString().trim(),
                        edtHargaBarang.getText().toString().trim(),
                        edtStokBarang.getText().toString().trim()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(UpdateActivity.this, "Sukses Update",
                                Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finishAffinity();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void initView() {
        iconItemName = findViewById(R.id.icon_item_name);
        edtNamaBarang = findViewById(R.id.edt_nama_barang);
        iconPriceTag = findViewById(R.id.icon_price_tag);
        edtHargaBarang = findViewById(R.id.edt_harga_barang);
        iconMinus = findViewById(R.id.icon_minus);
        iconPlus = findViewById(R.id.icon_plus);
        edtStokBarang = findViewById(R.id.edt_stok_barang);
        deskripsiBarang = findViewById(R.id.deskripsi_barang);
        ivPhoto = findViewById(R.id.iv_photo);
        edtUrlGambar = findViewById(R.id.edt_url_gambar);
        btnCheckUrl = findViewById(R.id.btn_check_url);
        btnKirim = findViewById(R.id.btn_kirim);
    }
}
