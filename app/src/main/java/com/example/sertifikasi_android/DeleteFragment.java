package com.example.sertifikasi_android;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sertifikasi_android.api.ApiConfig;
import com.example.sertifikasi_android.api.ApiService;
import com.example.sertifikasi_android.model.ProdukModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteFragment extends Fragment {

    private MenampilkanDeleteAdapter menampilkanDeleteAdapter;
    private ArrayList<ProdukModel> produkModels;
    private RecyclerView RVItem;

    public DeleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete, container, false);
        initView(view);

        getDataFromServer();
        return view;
    }

    private void getDataFromServer(){
        produkModels = new ArrayList<>();
        ApiService apiService = ApiConfig.getApiService();
        apiService.ambilData().enqueue(new Callback<ArrayList<ProdukModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ProdukModel>> call, Response<ArrayList<ProdukModel>> response) {
                if (response.isSuccessful()){
                    produkModels.clear();
                    produkModels = response.body();
                    menampilkanDeleteAdapter = new MenampilkanDeleteAdapter(getActivity(), produkModels);
                    RVItem.setAdapter(menampilkanDeleteAdapter);
                    RVItem.setLayoutManager(new GridLayoutManager(getActivity(),2));
                    menampilkanDeleteAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProdukModel>> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        RVItem = view.findViewById(R.id.RVItem);
    }
}
